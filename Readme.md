# Hubsites Plugin for Hubzilla


[Hubzilla](https://grid.reticu.li/hubzilla) is a powerful platform for creating interconnected websites featuring a decentralized identity, communications, and permissions framework built using common webserver technology. Hubzilla supports identity-aware webpages, using a modular system of website construction using shareable elements including pages, layouts, menus, and blocks. This plugin defines a schema for defining webpage elements in a typical git repository folder structure for external development of Hubzilla webpages as well as tools to simplify and automate deployment on a hub.

## Git repo schema

The git repo schema compatible with the Hubsites plugin is described below.
### Folder structure
Element definitions must be stored in the repo root under folders called 

```
/pages/
/blocks/
/layouts/
```

Each element of these types must be defined in an individual subfolder using two files: one JSON-formatted file for the metadata and one plain text file for the element content.

### Page elements
Page element metadata is specified in a JSON-formatted file called `page.json` with the following properties:

 * title
 * pagelink
 * mimetype
 * layout
 * contentfile

Example: 

```
/pages/my-page/page.json
/pages/my-page/my-page.bbcode
```

```
{
    "title": "My Page",
    "pagelink": "mypage",
    "mimetype": "text/bbcode",
    "layout": "my-layout",
    "contentfile": "my-page.bbcode"
}
```

### Layout elements
Layout element metadata is specified in a JSON-formatted file called `layout.json` with the following properties:

 * name
 * description
 * contentfile

Example: 

```
/layouts/my-layout/layout.json
/layouts/my-layout/my-layout.bbcode
```

```
{
    "name": "my-layout",
    "description": "Layout for my project page",
    "contentfile": "my-layout.bbcode"
}
```

### Block elements
Block element metadata is specified in a JSON-formatted file called `block.json` with the following properties:

 * name
 * title
 * mimetype
 * contentfile

Example: 

```
/blocks/my-block/block.json
/blocks/my-block/my-block.html
```

```
{
    "name": "my-block",
    "title": "",
    "mimetype": "text/html",
    "contentfile": "my-block.html"
}
```